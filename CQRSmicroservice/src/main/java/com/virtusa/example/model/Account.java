package com.virtusa.example.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name="Account")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Account {

	@Id
	private int id;

	public Account(int id2, String name2, int accountNo2, int balance2) {
	}
	
	public Account() {
		
	}

	public int getId(){
		return id;
	}

	public void setId(int id){
		this.id = id;
	}


	

		
		
		private String  name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	

		
		
		private int  accountNo;

		public int getAccountNo() {
			return accountNo;
		}

		public void setAccountNo(int accountNo) {
			this.accountNo = accountNo;
		}

	

		
		
		private int  balance;

		public int getBalance() {
			return balance;
		}

		public void setBalance(int balance) {
			this.balance = balance;
		}

	
	
}
