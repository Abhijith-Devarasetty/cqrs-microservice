package com.virtusa.example.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtusa.example.dao.AccountDao;
import com.virtusa.example.model.Account;

@Service
public class getAccountService {
	
	@Autowired
	private AccountDao dao;

	public List<Account> getAll(){
		return dao.findAll();
	}
	
	public Optional<Account> getById(int Id) {
		return dao.findById(Id);
	}
	
}
