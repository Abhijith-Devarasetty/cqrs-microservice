package com.virtusa.example.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtusa.example.dao.AccountDao;
import com.virtusa.example.model.Account;

@Service
public class createAccountService {

	@Autowired
	private AccountDao dao;
	
	public Account create
	(
		int id,	String  name,
		
		int  accountNo,
		
		int  balance
		
	) 
	{
		return dao.save(new Account(id,name,accountNo,balance));
	}
	 
	public Account update
	(int id,String  name,int  accountNo,int  balance){
		Account obj = dao.findById(id).orElse(new Account());
		obj.setName(name);
		obj.setAccountNo(accountNo);
		obj.setBalance(balance);
		return dao.save(obj);
	}
	
	public  void deleteAll() {
		dao.deleteAll();
	}
	
	public void deleteById(int Id) {
		Account obj = dao.findById(Id).orElse(new Account());
		dao.delete(obj);
	}
}
