package com.virtusa.example.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.virtusa.example.model.Account;
import com.virtusa.example.service.createAccountService;
import com.virtusa.example.service.getAccountService;

@Controller
public class AccountController {

	@Autowired
	private getAccountService getService;
	
	@Autowired
	private createAccountService createService;
	
	@RequestMapping("/create")
	@ResponseBody
	public String create(
		// @RequestParam String accountNo,@RequestParam int Id,@RequestParam String balance
		@RequestParam int Id,
		
			@RequestParam String  name,
		
			@RequestParam int  accountNo,
		
			@RequestParam int  balance
		
	) 
	{
		Account a = createService.create(
			// accountNo, balance, Id
			Id,
			
				name,
			
				accountNo,
			
				balance
			
		);
		return a.toString();
	}
	
	@RequestMapping("/getAll")
	@ResponseBody
	public List<Account> getAll() {
		return getService.getAll();
	}
	
	@RequestMapping("/getById")
	@ResponseBody
	public Optional<Account> getById(@RequestParam int id) {
		return getService.getById(id);
	}
	
	@RequestMapping("deleteAll")
	@ResponseBody
	public String deleteAll() {
		System.out.println("in create service");
		createService.deleteAll();
		return "All Records Deleted";
	}
	
	@RequestMapping("deleteById")
	@ResponseBody
	public String deleteById(@RequestParam int id) {
		createService.deleteById(id);
		return "Deleted record with Id " + id;
	}
	
	@RequestMapping("update")
	@ResponseBody
	public String update(
		// @RequestParam int aId,@RequestParam String accountNo,@RequestParam String balance
		@RequestParam int Id,
		
			@RequestParam String  name,
		
			@RequestParam int  accountNo,
		
			@RequestParam int  balance
		
	) 
	{
		Account a = createService.update(
			// accountNo, balance, aId
			Id,
			
				name,
			
				accountNo,
			
				balance
			
		);
		return a.toString();
	}
}
