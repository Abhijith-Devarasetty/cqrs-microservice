package com.virtusa.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.virtusa.example.model.Account;


public interface AccountDao extends JpaRepository<Account, Integer>{
	

}
