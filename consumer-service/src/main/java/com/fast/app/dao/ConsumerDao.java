package com.fast.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fast.app.model.Consumer;

public interface ConsumerDao extends JpaRepository<Consumer, Integer>{

}
