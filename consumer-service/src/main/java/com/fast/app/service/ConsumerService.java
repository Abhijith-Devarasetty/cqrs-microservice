package com.fast.app.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;


import com.fast.app.dao.ConsumerDao;
import com.fast.app.model.Consumer;

@Service
public class ConsumerService {

    @Autowired
	RestTemplate restTemplate;

	@Autowired
	private ConsumerDao dao;

	public List<Consumer> getAllConsumers(){
		return dao.findAll();
	}

	public Optional<Consumer> getConsumerById(int id) {
		return dao.findById(id);
	}

	public Consumer addConsumer(Consumer consumer) {
		return dao.save(consumer);
	}

	public void deleteConsumerById(int id) {
		dao.deleteById(id);;
	}

	public Consumer updateConsumer(Consumer consumer,int id){
		Consumer o = dao.findById(id).orElse(new Consumer());
			
			o.setFirstName(consumer.getFirstName());
			
			o.setLastName(consumer.getLastName());
			
			o.setGender(consumer.getGender());
			
			o.setAge(consumer.getAge());
			
			o.setAddress(consumer.getAddress());
			
			o.setPhoneNumber(consumer.getPhoneNumber());
		
		return dao.save(o);
	}
	
    
}
