package com.fast.app.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;


import com.fast.app.service.ConsumerService;
import com.fast.app.model.Consumer;

@Controller
@RefreshScope
@RequestMapping("/consumer-service")
@Api(value = "consumer-service")
public class ConsumerController {
	
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RestTemplate restTemplate;
	

	@Autowired
	private ConsumerService service;


	@GetMapping("/consumers")
	@ApiOperation(value = "Get All consumer api")
	@ResponseBody
	public List<Consumer> getConsumer(){
		log.info("in get all consumer method");
		return service.getAllConsumers();
	}

	@GetMapping("/consumer/{id}")
	@ApiOperation(value = "Get consumer by ID api")
	@ResponseBody
	public Optional<Consumer> getConsumerById(@PathVariable int id) {
		log.info("find by Id method");
		return service.getConsumerById(id);
	}

	@PostMapping("/consumer")
	@ApiOperation(value = "Post consumer api")
	@ResponseBody
	public Consumer addConsumer(@RequestBody Consumer consumer) {
		log.info("in create consumer");
		return service.addConsumer(consumer);
	}

	@DeleteMapping("/consumer/{id}")
	@ApiOperation(value = "Delete consumer by ID api")
	@ResponseBody
	public Map<String, String> deleteConsumerById(@PathVariable int id) {
		log.info("find all method");
		service.deleteConsumerById(id);
		HashMap<String,String> res = new HashMap<String, String>();
        res.put("message","done");
		return  res;
	}

	@PutMapping("/consumer/{id}")
	@ApiOperation(value = "Update consumer api")
	@ResponseBody
	public Consumer updateConsumer(@RequestBody Consumer consumer,@PathVariable int id){
		return service.updateConsumer(consumer,id);
	}

}
