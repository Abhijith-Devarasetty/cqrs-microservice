package com.fast.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Consumer {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
		private String firstName;

		public String getFirstName(){
			return firstName;
		}

		public void setFirstName(String firstName){
			this.firstName = firstName;
		}
	
		private String lastName;

		public String getLastName(){
			return lastName;
		}

		public void setLastName(String lastName){
			this.lastName = lastName;
		}
	
		private String gender;

		public String getGender(){
			return gender;
		}

		public void setGender(String gender){
			this.gender = gender;
		}
	
		private int age;

		public int getAge(){
			return age;
		}

		public void setAge(int age){
			this.age = age;
		}
	
		private String Address;

		public String getAddress(){
			return Address;
		}

		public void setAddress(String Address){
			this.Address = Address;
		}
	
		private String phoneNumber;

		public String getPhoneNumber(){
			return phoneNumber;
		}

		public void setPhoneNumber(String phoneNumber){
			this.phoneNumber = phoneNumber;
		}
	
	
	public Consumer() {  }

	public Consumer(
		
		int id,
		
			
			String firstName,
		
			
			String lastName,
		
			
			String gender,
		
			
			int age,
		
			
			String Address,
		
			
			String phoneNumber
		
	) {
		
			this.firstName = firstName;
		
			this.lastName = lastName;
		
			this.gender = gender;
		
			this.age = age;
		
			this.Address = Address;
		
			this.phoneNumber = phoneNumber;
		
	}


}
