package com.fast.app;


import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;



import com.fast.app.service.ConsumerService;
import com.fast.app.model.Consumer;
import com.fast.app.dao.ConsumerDao;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsumerServiceTests {
    
    @Autowired
	private ConsumerService service;

	@MockBean
	private ConsumerDao dao;

	@Test
	public void getAllConsumerTest() {
		when(dao.findAll()).thenReturn(Stream
				.of(new Consumer(
					// --TO DO 
					// please write the entity required feilds
					)).collect(Collectors.toList()));
		assertEquals(1, service.getAllConsumers().size());
	}

	@Test
	public void getConsumerByIdTest() {
		int id = 1;
		Optional<Consumer>  consumer= Optional.of(new Consumer());
		when(service.getConsumerById(id))
				.thenReturn(consumer);
		assertEquals(consumer, service.getConsumerById(id));
	}

	@Test
	public void addConsumerTest() {
		// TO DO
		// please write the entity required feilds 
		// example
		// User user = new User(999, "Pranya", 33, "Pune");
		Consumer consumer = new Consumer();
		when(service.addConsumer(consumer)).thenReturn(consumer);
		assertEquals(consumer, service.addConsumer(consumer));
	}

	@Test
	public void deleteConsumerByIdTest() {
		// TO DO 
		// please write the entity required feilds 
		// example
		// User user = new User(999, "Pranya", 33, "Pune");
		Consumer consumer = new Consumer();
		service.deleteConsumerById(consumer.getId());
		verify(dao, times(1)).deleteById(consumer.getId());
	}

	@Test
	public void updateConsumerTest() {
		// TO DO
		// please write the entity required feilds 
		// example
		// User user = new User(999, "Pranya", 33, "Pune");
		Consumer consumer = new Consumer();
		when(dao.save(consumer)).thenReturn(consumer);
		assertEquals(consumer, dao.save(consumer));
	}
}
