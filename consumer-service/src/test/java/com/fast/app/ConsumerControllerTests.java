package com.fast.app;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.fast.app.controller.ConsumerController;
import com.fast.app.model.Consumer;
import com.fast.app.service.ConsumerService;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ConsumerControllerTests {
	
	@Mock
	ConsumerService Service;
	
	@InjectMocks
	ConsumerController Controller;
	
	@Test
	public void getConsumerTest() {
		when(Service.getAllConsumers()).thenReturn(Stream
				.of(new Consumer(1,"Abhi","gupta","male",12,"hyd","123445"),
					new Consumer(1,"Abhi","gupta","male",12,"hyd","123445")).collect(Collectors.toList()));
		assertEquals(2, Controller.getConsumer().size());
	}
	
	@Test
	public void getConsumerByIdTest() {
		int id = 1;
		Optional<Consumer> consumer= Optional.of(new Consumer(1,"Abhi","gupta","male",12,"hyd","123445"));
		when(Service.getConsumerById(id))
				.thenReturn(consumer);
		assertEquals(consumer, Controller.getConsumerById(id));
	}
	
	@Test
	public void addConsumerTest() {
		Consumer consumer = new Consumer(1,"Abhi","gupta","male",12,"hyd","123445");
		when(Service.addConsumer(consumer)).thenReturn(consumer);
		assertEquals(consumer, Controller.addConsumer(consumer));
	}

	@Test
	public void deleteConsumerByIdTest() {
		Consumer consumer = new Consumer(1,"Abhi","gupta","male",12,"hyd","123445");
		Controller.deleteConsumerById(consumer.getId());
	 	verify(Service, times(1)).deleteConsumerById(consumer.getId());
	}
	
	@Test
	public void updateConsumerTest() {
		Consumer consumer = new Consumer(1,"Abhi","gupta","male",12,"hyd","123445");
		when(Service.updateConsumer(consumer,consumer.getId())).thenReturn(consumer);
		assertEquals(consumer, Controller.updateConsumer(consumer,consumer.getId()));
	}
}
