ObjectMapper objectMapper = new ObjectMapper();
        //For Date transformation
        objectMapper.findAndRegisterModules();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        JavaTimeModule module = new JavaTimeModule();
        objectMapper.registerModule(module);
        objectMapper.setTimeZone(TimeZone.getTimeZone("UTC/Greenwich"));
        //For Date transformation

 

        return objectMapper.writeValueAsString(object);


 List<FeatureRequest> featureRequestList = new ArrayList<>();
        featureRequestList.add(featureRequest1);
        featureRequestList.add(featureRequest2);
       
   
        Page<FeatureRequest> pagedTasks = new PageImpl(featureRequestList);
        Mockito.when(repo.findAll(org.mockito.Matchers.isA(Pageable.class))).thenReturn(pagedTasks);
       
        assertThat(service.getAllFeatureRequest(0,10,"FeatureId")).isEqualTo(pagedTasks);
    }

Page<FeatureRequest> pagedResponse = new PageImpl(featureRequestList);
        Mockito.when(service.getAllFeatureRequest(0,10,"FeatureId")).thenReturn(pagedResponse);
        
        String URI = "/featureRequests";
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                URI).accept(
                MediaType.APPLICATION_JSON);

 

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

 

        String expectedJson = this.mapToJson(pagedResponse);
        String outputInJson = result.getResponse().getContentAsString();
        assertThat(outputInJson).isEqualTo(expectedJson);