package com.virtusa.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.virtusa.demo.model.Customer;


public interface CustomerDao extends JpaRepository<Customer, Integer>{
	

}
