package com.virtusa.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.virtusa.demo.model.Customer;
import com.virtusa.demo.service.createCustomerService;
import com.virtusa.demo.service.getCustomerService;

@Controller
public class CustomerController {

	@Autowired
	private getCustomerService getService;
	
	@Autowired
	private createCustomerService createService;
	
	@RequestMapping("/create")
	@ResponseBody
	public String create(
		@RequestParam int Id,
		
			@RequestParam String  firstName,
		
			@RequestParam int  accountNo,
		
			@RequestParam int  balance,
		
			@RequestParam String  accountType,
		
			@RequestParam String  location,
		
	) 
	{
		Account a = createService.create(
			Id,
			
				firstName,
			
				accountNo,
			
				balance,
			
				accountType,
			
				location,
			
		);
		return a.toString();
	}
	
	@RequestMapping("/getAll")
	@ResponseBody
	public List<Customer> getAll() {
		return getService.getAll();
	}
	
	@RequestMapping("/getById")
	@ResponseBody
	public Optional<Customer> getById(@RequestParam int id) {
		return getService.getById(id);
	}
	
	@RequestMapping("deleteAll")
	@ResponseBody
	public String deleteAll() {
		System.out.println("in create service");
		createService.deleteAll();
		return "All Records Deleted";
	}
	
	@RequestMapping("deleteById")
	@ResponseBody
	public String deleteById(@RequestParam int id) {
		createService.deleteById(id);
		return "Deleted record with Id " + id;
	}
	
	@RequestMapping("update")
	@ResponseBody
	public String update(
		@RequestParam int Id,
		
			@RequestParam String  firstName,
		
			@RequestParam int  accountNo,
		
			@RequestParam int  balance,
		
			@RequestParam String  accountType,
		
			@RequestParam String  location,
		
	) 
	{
		Account a = createService.update(
			Id,
			
				firstName,
			
				accountNo,
			
				balance,
			
				accountType,
			
				location,
			
		);
		return a.toString();
	}
}
