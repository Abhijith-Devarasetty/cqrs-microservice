package com.virtusa.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtusa.demo.cqrs.dao.CustomerDao;
import com.virtusa.demo.model.Customer;

@Service
public class getCustomerService {
	
	@Autowired
	private CustomerDao dao;

	public List<Customer> getAll(){
		return dao.findAll();
	}
	
	public Optional<Customer> getById(int Id) {
		return dao.findById(Id);
	}
	
}
