package com.virtusa.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virtusa.demo.dao.CustomerDao;
import com.virtusa.demo.model.Customer;

@Service
public class createCustomerService {

	@Autowired
	private CustomerDao dao;
	
	public Customer create
	(
		int id,
		
		
		
		String  firstName,
		
		
		int  accountNo,
		
		
		int  balance,
		
		
		String  accountType,
		
		
		String  location
		
	) 
	{
		return dao.save(new Customer (id,firstName,accountNo,balance,accountType,location));
	}
	
	public Customer update
	(
		int id,
		
		
		
		String  firstName,
		
		
		int  accountNo,
		
		
		int  balance,
		
		
		String  accountType,
		
		
		String  location
		
	) 
	{
		Customer obj = dao.findById(id).orElse(new Customer());
		
			obj.setFirstName(firstName);
		
			obj.setAccountNo(accountNo);
		
			obj.setBalance(balance);
		
			obj.setAccountType(accountType);
		
			obj.setLocation(location);
		

		return dao.save(obj);
	}
	
	public  void deleteAll() {
		dao.deleteAll();
	}
	
	public void deleteById(int Id) {
		Customer obj = dao.findById(Id).orElse(new Customer());
		dao.delete(obj);
	}
}
