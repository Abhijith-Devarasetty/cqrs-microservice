package com.virtusa.demo.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name="Customer")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Customer {

	@Id
	private int id;

	public int getId(){
		return id;
	}

	public void setId(int id){
		this.id = id;
	}


	

		
		
		private String  firstName;

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

	

		
		
		private int  accountNo;

		public int getAccountNo() {
			return accountNo;
		}

		public void setAccountNo(int accountNo) {
			this.accountNo = accountNo;
		}

	

		
		
		private int  balance;

		public int getBalance() {
			return balance;
		}

		public void setBalance(int balance) {
			this.balance = balance;
		}

	

		
		
		private String  accountType;

		public String getAccountType() {
			return accountType;
		}

		public void setAccountType(String accountType) {
			this.accountType = accountType;
		}

	

		
		
		private String  location;

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

	

	public Customer(){
		
	}

	public Customer(
			
			int id,
		
			String  firstName,
		
			int  accountNo,
		
			int  balance,
		
			String  accountType,
		
			String  location
		
	){
		
	}
	
}
